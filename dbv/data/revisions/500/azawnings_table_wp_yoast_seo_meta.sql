
-- --------------------------------------------------------

--
-- Table structure for table `wp_yoast_seo_meta`
--

DROP TABLE IF EXISTS `wp_yoast_seo_meta`;
CREATE TABLE IF NOT EXISTS `wp_yoast_seo_meta` (
  `object_id` bigint(20) UNSIGNED NOT NULL,
  `internal_link_count` int(10) UNSIGNED DEFAULT NULL,
  `incoming_link_count` int(10) UNSIGNED DEFAULT NULL,
  UNIQUE KEY `object_id` (`object_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_yoast_seo_meta`
--

INSERT INTO `wp_yoast_seo_meta` (`object_id`, `internal_link_count`, `incoming_link_count`) VALUES
(4, 0, 0),
(6, 0, 0),
(7, 0, 0),
(117, 0, 0),
(155, 0, 0),
(156, 0, 0),
(158, 0, 0),
(162, 0, 0),
(166, 0, 0),
(172, 0, 0),
(176, 0, 0),
(181, 0, 0),
(187, 0, 0),
(193, 0, 0),
(198, 0, 0),
(203, 0, 0),
(210, 0, 0),
(212, 0, 0),
(329, 0, 0),
(344, 0, 0),
(353, 0, 0),
(355, 0, 0);
