
-- --------------------------------------------------------

--
-- Table structure for table `wp_ngg_album`
--

DROP TABLE IF EXISTS `wp_ngg_album`;
CREATE TABLE IF NOT EXISTS `wp_ngg_album` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `previewpic` bigint(20) NOT NULL DEFAULT '0',
  `albumdesc` mediumtext COLLATE utf8mb4_unicode_ci,
  `sortorder` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `pageid` bigint(20) NOT NULL DEFAULT '0',
  `extras_post_id` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `extras_post_id_key` (`extras_post_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `wp_ngg_album`
--

TRUNCATE TABLE `wp_ngg_album`;
--
-- Dumping data for table `wp_ngg_album`
--

INSERT INTO `wp_ngg_album` (`id`, `name`, `slug`, `previewpic`, `albumdesc`, `sortorder`, `pageid`, `extras_post_id`) VALUES
(1, 'Old Projects', 'old-projects', 1, '', 'WyIzIl0=', 0, 153);
