
-- --------------------------------------------------------

--
-- Table structure for table `wp_yoast_seo_links`
--

DROP TABLE IF EXISTS `wp_yoast_seo_links`;
CREATE TABLE IF NOT EXISTS `wp_yoast_seo_links` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_id` bigint(20) UNSIGNED NOT NULL,
  `target_post_id` bigint(20) UNSIGNED NOT NULL,
  `type` varchar(8) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `link_direction` (`post_id`,`type`)
) ENGINE=InnoDB AUTO_INCREMENT=180 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_yoast_seo_links`
--

INSERT INTO `wp_yoast_seo_links` (`id`, `url`, `post_id`, `target_post_id`, `type`) VALUES
(127, 'tel:4803992336', 158, 0, 'external'),
(128, 'mailto:azshadengates@gmail.com', 158, 0, 'external'),
(133, 'http://www.thelightstrip.com/', 181, 0, 'external'),
(134, 'http://www.thelightstrip.com/', 181, 0, 'external'),
(135, 'https://www.alumawood.net/products_color_options.php', 181, 0, 'external'),
(136, 'https://www.alumawood.net/dealers_locator.php', 181, 0, 'external'),
(151, 'tel:4803992336', 162, 0, 'external');
