
-- --------------------------------------------------------

--
-- Table structure for table `wp_ngg_gallery`
--

DROP TABLE IF EXISTS `wp_ngg_gallery`;
CREATE TABLE `wp_ngg_gallery` (
  `gid` bigint(20) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `path` mediumtext COLLATE utf8mb4_unicode_ci,
  `title` mediumtext COLLATE utf8mb4_unicode_ci,
  `galdesc` mediumtext COLLATE utf8mb4_unicode_ci,
  `pageid` bigint(20) NOT NULL DEFAULT '0',
  `previewpic` bigint(20) NOT NULL DEFAULT '0',
  `author` bigint(20) NOT NULL DEFAULT '0',
  `extras_post_id` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `wp_ngg_gallery`
--

TRUNCATE TABLE `wp_ngg_gallery`;
--
-- Dumping data for table `wp_ngg_gallery`
--

INSERT INTO `wp_ngg_gallery` (`gid`, `name`, `slug`, `path`, `title`, `galdesc`, `pageid`, `previewpic`, `author`, `extras_post_id`) VALUES
(1, 'project-showcase', 'Project-Showcase', 'wp-content\\\\gallery\\\\project-showcase/', 'Project Showcase', '', 0, 1, 1, 128),
(2, 'project-showcase', 'project-showcase-1', 'wp-content/gallery/project-showcase-1/', 'project-showcase', NULL, 0, 25, 1, 196),
(3, 'az-shade-n-gates-awnings', 'AZ-Shade-n-Gates-Awnings', 'wp-content/gallery/az-shade-n-gates-awnings/', 'AZ Shade n Gates Awnings', NULL, 0, 26, 1, 222);
