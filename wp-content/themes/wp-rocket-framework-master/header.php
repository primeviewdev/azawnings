<!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) & !(IE 8)]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head> 
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title><?php wp_title('|','true','right'); ?></title>
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<link rel="icon" href="<?=get_option('favicon');?>" type="image/x-icon" />
<?php // Loads HTML5 JavaScript file to add support for HTML5 elements in older IE versions. ?>
<!--[if lt IE 9]>
<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js" type="text/javascript"></script>
<![endif]-->
<?php wp_head(); ?>
</head>
<body <?php body_class(); ?> >
	<div class="loading"><img class="img-fluid" src="<?php echo get_template_directory_uri(); ?>/assets/img/loading.gif" alt="Loading..." title="Loading..."></div>
	<div id="page" class="hfeed site">
		<nav class="navbar navbar-expand-lg navbar-light bg-light">
			<div class="container">
				<div class="logo-wrap"><a href="/"><img src="/wp-content/uploads/2019/04/bw-logo-sm.png" /></a></div>
			<div class="header-text">
				<p><img src="/wp-content/uploads/2019/05/Phone-Icon.png"/><a href="tel:4803957710">Call Now 480.395.7710</a></p>
				<p><a href="mailto:azshadengates@gmail.com">AZShadeNGates@gmail.com</a></p>
						
				
			</div>
				

				<?php echo do_shortcode('[rocketmenu]'); ?>
			</div>
		</nav>