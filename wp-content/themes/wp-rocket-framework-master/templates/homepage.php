<?php
/**
 * Template Name: Front Page Template
 *
 */
get_header(); ?>
	<div id="primary" class="homepage site-content">
	
		<div class="jumbotron">
			<div class="container">
				<div class="row" role="main">
					<div class="col-md-12">		
						<?php 
echo do_shortcode('[smartslider3 slider=2]');
?>
					</div>
				</div>
			</div>
		</div>
		
		<div class="container">
			<div class="row" role="main">
				<div class="col-md-12">
					<?php 
						while (have_posts()){ 
							the_post(); 
							get_template_part( 'content', 'page' ); 
						} 
					?>					
				</div><!-- .col-md-12 -->
			</div><!--.row -->
		</div><!-- .container-->
		
		
		
	</div><!-- .primary -->
<?php get_footer(); ?>