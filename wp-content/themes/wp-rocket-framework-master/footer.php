			<footer class="footer">
			  <div class="container">
				<div class="row footer">
							<div class="col-md-4">
								<h4>AZShadeNGates.com</h4>
									<ul>
										<li><a href="/contact-us">Contact Us</a></li>
										<li><a href="/request-a-bid">Quote Request</a></li>
										<li><a href="/privacy-policy">Privacy Policy</a></li>
										<li><a href="/project-showcase">Project Showcase</a></li>
										<li><a href="/about-us">About Us</a></li>
									</ul>
							</div>
						
							<div class="col-md-4">
								<h4>Awnings Installed today</h4>
								<p>AZ Shade 'n Gates LLC</p>
								<p>Call Us Today!!!  <a href="tel:4803957710">480.395.7710</a></p>
								<p><a href="mailto:azshadengates@gmail.com">AZShadeNGates@gmail.com</a></p>
							</div>
							<div class="col-md-4">
								<h4>We Proudly Use Alumawood!</h4>
							<img src="/wp-content/uploads/2019/05/logo_alumawood.gif"/>
							</div>
						</div>
					<div class="row footer">
						<div class="copyright">
						<p>copyright 2019 Az Shade n' Gates LLC <a href="optimizex.com">optimization by optimizex</a> <a href="primeview.com">Web Design by Primeview</a></p>
						</div>
					</div>
			  </div>
			</footer>
		</div>
		<?php wp_footer(); ?>
		<?php
			if(get_option('rocket_scripts')!=null){
				echo get_option("rocket_scripts");		
			}
		?>
	</body>
</html>